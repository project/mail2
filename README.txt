Please read this file and also the INSTALL.txt.

** Description:
The mail2 module provides submissions forms for your mail2 newsletters.
The forms are generated automatically and are made available as blocks
on your site for you to place where you want.

** Installation AND Upgrades:
See the INSTALL.txt file.

** Notices:
The mail2 module just creates subscription forms for newsletters you
have already configured at the mail2 site. You can not edit them using
this module, you can only subscribe and unsubscribe to existing
newsletters.

** Credits:
Marcus Månsson main developer
Erik Johansson additions and code reviews
Olof Johansson feedback and code reviews

Current maintainers:
  Odd Hill - http://www.oddhill.se
